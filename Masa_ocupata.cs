﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewYorkPub
{
    public partial class Masa_ocupata : Form
    {
        public Masa_ocupata()
        {
            InitializeComponent();
        }

        public void label3_Click(object sender, EventArgs e)
        {
            label3.Text = Form1.suma_bani.ToString() + " lei";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1.suma_bani = 0;
            Close();
        }
    }
}
